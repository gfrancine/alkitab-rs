mod books;
use books::new_books;

mod queries;
use queries::Query;

fn main() {
  let mut args = std::env::args();
  let _ = args.next();
  let q_arg = args.next().unwrap();

  match q_arg.as_str() {
    "index" => {
      let mut keys = new_books().into_keys().collect::<Vec<&str>>();
      keys.sort();
      for k in keys.iter() {
        println!("{}", k);
      }
    }
    _ => {
      let query =
        Query::from_str(&format!("{} {}", &q_arg, args.next().unwrap()))
          .unwrap();
      match query.run(&new_books()) {
        Ok(result) => println!("{}", result.to_formatted_string()),
        Err(err) => eprintln!("{:?}", err),
      }
    }
  };
}
