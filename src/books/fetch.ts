// deno run --allow-write --allow-net fetch.ts

import { parse } from "https://deno.land/x/xml@v1.0.2/mod.ts";

type PassageListEntry = {
  book_number: number;
  abbreviation: string;
  book_name: string;
  total_chapter: number;
};

type DataVerse = {
  title?: string,
  number: number,
  text: string,
}

type DataChapter = {
  jsonVerses: string // DataVerse[]
}

type DataBook = {
  title: string,
  chapters: DataChapter[],
}

const passagesResponse = await fetch("https://api-alkitab.herokuapp.com/v2/passage/list");

const passages: {
  passage_list: PassageListEntry[];
} = JSON.parse(await passagesResponse.text());

const normalizeBookName = (name: string) => name.toLowerCase().replace(" ", "");

async function fetchBook(passage: PassageListEntry) {
  console.log("Fetching book", passage.abbreviation);
  const book: DataBook = {
    title: passage.book_name,
    chapters: [],
  };

  type SabdaChapter = {
    bible: {
      verses: {
        verse: DataVerse[]
      }
    }
  };

  for (let n = 1; n < passage.total_chapter + 1; n++) {
    console.log("Fetching chapter", n);
    const url = `https://alkitab.sabda.org/api/chapter.php?book=${passage.book_number}&chapter=${n}`;
    const xml: SabdaChapter = await parse(await fetch(url).then((response) => response.text())) as SabdaChapter;

    const chapter: DataChapter = {
      jsonVerses: JSON.stringify(xml.bible.verses.verse)
    }

    book.chapters.push(chapter);
  }

  Deno.writeTextFileSync(
    `./json/${normalizeBookName(passage.abbreviation)}.json`,
    JSON.stringify(book),
  );
}

const promises: Promise<void>[] = [];
for (let i = 0; i < passages.passage_list.length; i++) {
  const passage = passages.passage_list[i];
  promises.push(fetchBook(passage));
}
await Promise.all(promises);