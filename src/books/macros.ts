// deno run --allow-net src/books/macros.ts | pbcopy

type PassageListEntry = {
  book_number: number;
  abbreviation: string;
  book_name: string;
  total_chapter: number;
};

const passagesResponse = await fetch("https://api-alkitab.herokuapp.com/v2/passage/list");

const passages: {
  passage_list: PassageListEntry[];
} = JSON.parse(await passagesResponse.text());

const normalizeBookName = (name: string) => name.toLowerCase().replace(" ", "");

// print out the macros so all we need to do is copy and paste to mod.rs
passages.passage_list.forEach((entry) => console.log(`book!("${
  normalizeBookName(entry.abbreviation)
}"),`))
