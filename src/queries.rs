use alkitab_rs::{BookRegistry, Verse};
use lazy_static::lazy_static;
use regex::Regex;
use std::fmt::Debug;

#[derive(PartialEq, Debug)]
pub enum QueryErr {
  BookNotFound,
  ChapterNotFound,
  VerseNotFound,
}

#[derive(PartialEq, Debug)]
pub enum QueryParseErr {
  NoMatch,
}

#[derive(Debug)]
pub struct QueryResult {
  book_title: String,
  chapter: usize,
  verse_start: Option<usize>,
  verse_end: Option<usize>,
  verses: Vec<Verse>,
}

impl QueryResult {
  pub fn to_formatted_string(&self) -> String {
    let title = format!(
      "{} {}{}{}",
      self.book_title,
      self.chapter,
      match self.verse_start {
        Some(verse_start) => format!(":{}", verse_start),
        None => "".to_string(),
      },
      match self.verse_end {
        Some(verse_end) => format!("-{}", verse_end),
        None => "".to_string(),
      }
    );

    let mut verses = String::from("");
    for verse in self.verses.iter() {
      verses.push_str("\r\n\r\n");
      if let Some(title) = &verse.title {
        verses.push_str(&title);
        verses.push_str("\r\n");
      }
      verses.push_str(&format!("{} - {}", verse.number, verse.text,));
    }

    format!("\r\n{}{}\r\n", title, verses)
  }
}

#[derive(Debug)]
pub struct Query {
  book: String,
  chapter: usize,
  verse_start: Option<usize>,
  verse_end: Option<usize>,
}

impl Query {
  pub fn run(
    &self,
    book_registry: &BookRegistry,
  ) -> Result<QueryResult, QueryErr> {
    // get the book
    let book = match book_registry.get(&self.book as &str) {
      Some(book) => book,
      None => return Err(QueryErr::BookNotFound),
    };

    // get the chapter
    let chapter = match book.chapters.get(self.chapter - 1) {
      Some(chapter) => chapter,
      None => return Err(QueryErr::ChapterNotFound),
    };

    // check if the verses exist
    let all_verses = chapter.verses().unwrap();

    let start: usize = match self.verse_start {
      None => 0,
      Some(index) => match all_verses.get(index - 1) {
        None => return Err(QueryErr::VerseNotFound),
        Some(_) => index - 1,
      },
    };

    let end: usize = match self.verse_end {
      None => match self.verse_start {
        Some(_) => start,
        None => all_verses.len() - 1,
      },
      Some(index) => match all_verses.get(index - 1) {
        None => return Err(QueryErr::VerseNotFound),
        Some(_) => index - 1,
      },
    };

    // slice the verses
    let verses = all_verses[start..=end].to_vec();

    Ok(QueryResult {
      book_title: book.title.clone(),
      chapter: self.chapter,
      verse_start: self.verse_start,
      verse_end: self.verse_end,
      verses,
    })
  }

  pub fn from_str(query: &str) -> Result<Self, QueryParseErr> {
    lazy_static! {
      static ref RE: Regex = Regex::new(
        r"(?P<book>\w+) (?P<chapter>\d+)(:(?P<verse_start>\d+)(-(?P<verse_end>\d+))?)?"
      ).unwrap();
    }

    let caps = match RE.captures(query) {
      Some(caps) => caps,
      None => return Err(QueryParseErr::NoMatch),
    };

    let book = caps.name("book").unwrap().as_str().to_string();

    let chapter = caps
      .name("chapter")
      .unwrap()
      .as_str()
      .parse::<usize>()
      .unwrap();

    let verse_start: Option<usize> = match caps.name("verse_start") {
      Some(re_match) => Some(re_match.as_str().parse::<usize>().unwrap()),
      None => None,
    };

    let verse_end: Option<usize> = match caps.name("verse_end") {
      Some(re_match) => Some(re_match.as_str().parse::<usize>().unwrap()),
      None => None,
    };

    Ok(Query {
      book,
      chapter,
      verse_start,
      verse_end,
    })
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use alkitab_rs::Book;
  use std::collections::HashMap;

  lazy_static! {
    pub static ref BOOKS: BookRegistry = HashMap::from([
      ("test", Book::new(r#"{"title": "Test Book","chapters": [{
        "jsonVerses": "[{\"number\":1,\"text\":\"text\",\"title\":\"hi\"},{\"number\":2,\"text\":\"text2\"}]"
      }]}"#).unwrap())
    ]);
  }

  #[test]
  fn query_from_string() {
    assert_eq!(
      Query::from_str("test a").unwrap_err(),
      QueryParseErr::NoMatch
    );

    let query = Query::from_str("test 1:a").unwrap();
    assert_eq!(query.verse_start, None);

    let query = Query::from_str("test 1:1-a").unwrap();
    assert_eq!(query.verse_end, None);

    let query = Query::from_str("1test 1:2-3").unwrap();
    assert_eq!(query.book, "1test".to_string());
    assert_eq!(query.chapter, 1);
    assert_eq!(query.verse_start, Some(2));
    assert_eq!(query.verse_end, Some(3));
  }

  #[test]
  fn query_run() {
    assert_eq!(
      Query::from_str("a 1").unwrap().run(&BOOKS).unwrap_err(),
      QueryErr::BookNotFound,
    );

    assert_eq!(
      Query::from_str("test 3").unwrap().run(&BOOKS).unwrap_err(),
      QueryErr::ChapterNotFound,
    );

    assert_eq!(
      Query::from_str("test 1:3")
        .unwrap()
        .run(&BOOKS)
        .unwrap_err(),
      QueryErr::VerseNotFound,
    );

    let result = Query::from_str("test 1:1").unwrap().run(&BOOKS).unwrap();

    assert_eq!(result.verses.len(), 1);
  }

  #[test]
  fn query_result_to_string() {
    let q_result = QueryResult {
      book_title: String::from("Book Title"),
      chapter: 1,
      verse_start: Some(2),
      verse_end: Some(3),
      verses: vec![
        Verse {
          title: Some(String::from("Verse Title")),
          number: 1,
          text: "Verse 1".to_string(),
        },
        Verse {
          title: None,
          number: 2,
          text: "Verse 2".to_string(),
        },
      ],
    };

    let expected = "\r\n\
      Book Title 1:2-3\r\n\r\n\
      Verse Title\r\n\
      1 - Verse 1\r\n\r\n\
      2 - Verse 2\r\n"
      .to_string();

    assert_eq!(q_result.to_formatted_string(), expected);
  }
}
