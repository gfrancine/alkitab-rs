use serde::Deserialize;
use std::collections::HashMap;
use std::fmt::Debug;

#[derive(Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Verse {
  pub title: Option<String>,
  pub number: usize,
  pub text: String,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Chapter {
  // the vector of verses are double encoded
  // so they're only parsed when needed
  json_verses: String,
}

impl Chapter {
  pub fn verses(&self) -> serde_json::Result<Vec<Verse>> {
    serde_json::from_str(&self.json_verses)
  }
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Book {
  pub title: String,
  pub chapters: Vec<Chapter>,
}

impl Book {
  pub fn new(json: &str) -> serde_json::Result<Self> {
    serde_json::from_str(json)
  }
}

pub type BookRegistry = HashMap<&'static str, Book>;

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn deserialize_book() {
    let json_book = r#"{"title": "Test Book","chapters": [{
      "jsonVerses": "[{\"number\":1,\"text\":\"text\",\"title\":\"hi\"},{\"number\":2,\"text\":\"text2\"}]"
    }]}"#;

    let book = Book::new(json_book).unwrap();
    let chapter = &book.chapters[0];
    let _ = chapter.verses().unwrap();
  }
}
